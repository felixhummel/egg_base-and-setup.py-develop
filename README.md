Create virtualenv:
```
pyenv virtualenv 3.7.4 eggy
```

This works:
```
python setup.py develop
cd /tmp/ && python -c 'import foo.bar'; cd -
```

This raises `ModuleNotFoundError: No module named 'foo.bar'`
```
cat <<EOF > setup.cfg
[egg_info]
egg_base = my_egg_base
EOF

python setup.py develop
cd /tmp/ && python -c 'import foo.bar'; cd -
```

